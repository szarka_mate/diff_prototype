﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DiffPrototype.Data.Migrations
{
    public partial class TrustContractTypoFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ConfindentalInformation",
                table: "TrustContracts",
                newName: "ConfidentalInformation");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ConfidentalInformation",
                table: "TrustContracts",
                newName: "ConfindentalInformation");
        }
    }
}
