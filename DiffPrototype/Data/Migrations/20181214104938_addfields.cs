﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DiffPrototype.Data.Migrations
{
    public partial class addfields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ConfidentalInformation",
                table: "TrustContracts",
                newName: "ConfidentaInformation");

            migrationBuilder.AddColumn<int>(
                name: "HandoverCount",
                table: "TrustContracts",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HandoverCount",
                table: "TrustContracts");

            migrationBuilder.RenameColumn(
                name: "ConfidentaInformation",
                table: "TrustContracts",
                newName: "ConfidentalInformation");
        }
    }
}
