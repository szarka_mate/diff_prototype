﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DiffPrototype.Data.Migrations
{
    public partial class addedField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TEnum",
                table: "TrustContracts",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TEnum",
                table: "TrustContracts");
        }
    }
}
