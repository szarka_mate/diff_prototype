﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DiffPrototype.Data.Migrations
{
    public partial class AddedFileData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "FileData",
                table: "TrustContracts",
                nullable: false,
                defaultValue: new byte[] {  });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FileData",
                table: "TrustContracts");
        }
    }
}
