﻿using System;
using System.Collections.Generic;
using System.Text;
using DiffPrototype.Areas.Identity.Data;
using DiffPrototype.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DiffPrototype.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<ContractFieldChange> ContractFieldChanges { get; set; }
        public DbSet<ContractChange> ContractChanges { get; set; }
        public DbSet<TrustContract> TrustContracts { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ContractChange>()
                .HasOne<TrustContract>(c => c.Contract)
                .WithMany(tc => tc.Changes)
                .HasForeignKey(c => c.ContractId);

            builder.Entity<ContractFieldChange>()
                .HasOne<ContractChange>(c => c.ContractChange)
                .WithMany(f => f.ContracFieldChanges)
                .HasForeignKey(c => c.ContractChangeId);

            builder.Entity<TrustContract>()
                .HasOne<ApplicationUser>(u => u.Creator)
                .WithMany(c => c.CreatedContracts)
                .HasForeignKey(u => u.CreatorId)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<TrustContract>()
                .HasOne<ApplicationUser>(u => u.Partner)
                .WithMany(c => c.ParticipatedContracts)
                .HasForeignKey(u => u.PartnerId)
                .OnDelete(DeleteBehavior.SetNull);

            base.OnModelCreating(builder);
        }
    }
}
