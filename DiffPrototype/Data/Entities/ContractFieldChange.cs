﻿using DiffPrototype.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DiffPrototype.Data.Entities
{
    public class ContractFieldChange
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public Guid ContractChangeId { get; set; }
        [ForeignKey("ContractChangeId")]
        public ContractChange ContractChange { get; set; }

        [Required]
        public string Field { get; set; }

        [Required]
        public string OldValue { get; set; }

        [Required]
        public string NewValue { get; set; }

        [Required]
        public ContractStep AtStep { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
