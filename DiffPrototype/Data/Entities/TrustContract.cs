﻿using DiffPrototype.Areas.Identity.Data;
using DiffPrototype.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DiffPrototype.Data.Entities
{
    public class TrustContract
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public int Price { get; set; }
        public float VAT { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime PaymentDate { get; set; }

        public string Description { get; set; }
        public string ConfidentaInformation { get; set; }

        public ContractType Type { get; set; }
        public ContractStatus Status { get; set; }

        public TestEnum TEnum { get; set; }

        [Required]
        public bool IsFulfilled { get; set; }

        public bool TestBoolFieldTwo { get; set; }

        public byte[] FileData { get; set; }

        public string FileName { get; set; }

        public string FileContentType { get; set; }

        [Required]
        public ContractStep AtStep { get; set; }
        public string CreatorId { get; set; }
        [ForeignKey("CreatorId")]
        public ApplicationUser Creator { get; set; }

        public string PartnerId { get; set; }
        [ForeignKey("PartnerId")]
        public ApplicationUser Partner { get; set; }

        public int HandoverCount { get; set; }

        public ICollection<ContractChange> Changes { get; set; }
    }
}
