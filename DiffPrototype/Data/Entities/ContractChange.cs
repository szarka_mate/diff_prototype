﻿using DiffPrototype.Areas.Identity.Data;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DiffPrototype.Data.Entities
{
    public class ContractChange
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public Guid ContractId { get; set; }
        [ForeignKey("ContractId")]
        public TrustContract Contract { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        [Required]
        public string CreatorId { get; set; }
        [ForeignKey("CreatorId")]
        public ApplicationUser Creator { get; set; }

        public ICollection<ContractFieldChange> ContracFieldChanges { get; set; }

    }
}
