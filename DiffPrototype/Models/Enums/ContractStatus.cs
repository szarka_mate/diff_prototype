﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiffPrototype.Models.Enums
{
    public enum ContractStatus
    {
        AtCreator,
        AtPartner,
        Finalized
    }
}
