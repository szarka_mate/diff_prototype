﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiffPrototype.Models.Enums
{
    public enum ContractType
    {
        // creator wants to buy
        Buy,
        // creator wants to sell
        Sell
    }
}
