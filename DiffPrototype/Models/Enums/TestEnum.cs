﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiffPrototype.Models.Enums
{
    public enum TestEnum
    {
        Case1,
        Case2,
        CaseN,
        CaseNPlus1
    }
}
