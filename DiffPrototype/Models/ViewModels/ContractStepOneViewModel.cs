﻿using DiffPrototype.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DiffPrototype.Models.ViewModels
{
    public class ContractStepOneViewModel : AdditionalContractInfoViewModel
    {
        [Required]
        public TestEnum TEnum { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public int Price { get; set; }

        [Required]
        public float VAT { get; set; }

        
        
    }
}
