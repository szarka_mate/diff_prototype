﻿using DiffPrototype.Areas.Identity.Data;
using DiffPrototype.Models.Enums;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DiffPrototype.Models.ViewModels
{
    public class AddContractViewModel
    {
        public string PartnerId { get; set; }
        public string PartnerName { get; set; }
        public int Price { get; set; }
        public float VAT { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true,DataFormatString = "yyyy.MM.dd")]
        public DateTime PaymentDate{ get; set; }
        public string Description { get; set; }
        public string ConfidentalInformation { get; set; }
        public Guid ContractId { get; set; }


        [EnumDataType(typeof(ContractType))]
        public ContractType Type { get; set; }

        [EnumDataType(typeof(ContractStatus))]
        public ContractStatus Status { get; set; }

        [EnumDataType(typeof(TestEnum))]
        public TestEnum TEnum { get; set; }
        //public bool IsFulfilled { get; set; }
        public bool TestBoolFieldTwo { get; set; }

        public IFormFile FileData { get; set; }

        public List<ApplicationUser> Users { get; set; }
    }
}
