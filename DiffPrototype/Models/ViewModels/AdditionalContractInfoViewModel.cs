﻿using DiffPrototype.Areas.Identity.Data;
using DiffPrototype.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DiffPrototype.Models.ViewModels
{
    public class AdditionalContractInfoViewModel
    {
        public List<ApplicationUser> Users { get; set; }
        [ReadOnly(true)]
        public ContractType Type { get; set; }
        [ReadOnly(true)]
        public string PartnerId { get; set; }
        [ReadOnly(true)]
        public string PartnerName { get; set; }
        [ReadOnly(true)]
        public DateTime CreatedAt { get; set; }
        [ReadOnly(true)]
        public bool IsFultilled { get; set; }
        [ReadOnly(true)]
        public Guid ContractId { get; set; }
        public bool NextPage { get; set; }

        public List<string> ChangedField { get; set; }
        public List<string> ChangedStep { get; set; }
    }
}
