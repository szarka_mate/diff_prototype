﻿using DiffPrototype.Areas.Identity.Data;
using DiffPrototype.Models.Enums;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DiffPrototype.Models.ViewModels
{
    public class EditContractViewModel
    {
        public Guid ContractId { get; set; }
        [ReadOnly(true)]
        public string PartnerId { get; set; }
        [ReadOnly(true)]
        public string PartnerName { get; set; }
        public int Price { get; set; }
        public float VAT { get; set; }
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy.MM.dd}")]
        public DateTime PaymentDate { get; set; }
        public string Description { get; set; }
        public string ConfidentalInformation { get; set; }

        [ReadOnly(true)]
        [EnumDataType(typeof(ContractType))]
        public ContractType Type { get; set; }

        [ReadOnly(true)]
        [EnumDataType(typeof(ContractStatus))]
        public ContractStatus Status { get; set; }

        [EnumDataType(typeof(TestEnum))]
        public TestEnum TEnum { get; set; }
        //public bool IsFulfilled { get; set; }
        public IFormFile FileDataForm { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] FileData { get; set; }
        public bool TestBoolFieldTwo { get; set; }

        [ReadOnly(true)]
        public List<ApplicationUser> Users { get; set; }
    }
}
