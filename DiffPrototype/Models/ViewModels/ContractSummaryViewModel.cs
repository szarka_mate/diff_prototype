﻿using DiffPrototype.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiffPrototype.Models.ViewModels
{
    public class ContractSummaryViewModel
    {
        public Guid ContractId { get; set; }
        public string Creator { get; set; }
        public string Partner { get; set; }
        public ContractType Type { get; set; }
        public ContractStatus Status { get; set; }
        public TestEnum TEnum { get; set; }
        public bool TestBoolField { get; set; }
        public string FileName { get; set; }
        public int Handovers { get; set; }
    }
}
