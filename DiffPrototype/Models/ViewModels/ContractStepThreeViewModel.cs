﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiffPrototype.Models.ViewModels
{
    public class ContractStepThreeViewModel : AdditionalContractInfoViewModel
    {

        public IFormFile FileData { get; set; }
        public string FileName { get; set; }

    }
}
