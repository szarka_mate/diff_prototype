﻿using DiffPrototype.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiffPrototype.Models.ViewModels
{
    public class ContractsViewModel
    {
        public List<TrustContract> TrustContracts { get; set; }
    }
}
