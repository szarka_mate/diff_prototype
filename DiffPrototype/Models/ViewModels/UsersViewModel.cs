﻿using DiffPrototype.Areas.Identity.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiffPrototype.Models.ViewModels
{
    public class UsersViewModel
    {
        public List<ApplicationUser> Users { get; set; }
    }
}
