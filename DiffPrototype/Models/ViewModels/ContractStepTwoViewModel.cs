﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiffPrototype.Models.ViewModels
{
    public class ContractStepTwoViewModel : AdditionalContractInfoViewModel
    {
        public DateTime PaymentDate { get; set; }

        public bool TestBoolFieldTwo { get; set; }

        public string ConfidentaInformation { get; set; }
    }
}
