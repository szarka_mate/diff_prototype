﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiffPrototype.Models
{
    public static class HtmlHelper
    {
        public static T Cast<T>(this object o)
        {
            return (T)o;
        }
    }
}
