﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DiffPrototype.Models;
using DiffPrototype.Data;
using Microsoft.AspNetCore.Identity;
using DiffPrototype.Data.Entities;
using DiffPrototype.Models.ViewModels;
using DiffPrototype.Areas.Identity.Data;
using Microsoft.AspNetCore.Authorization;
using DiffPrototype.Models.Enums;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using System.IO;
using System.Reflection;

namespace DiffPrototype.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public HomeController(
            ApplicationDbContext context,
            UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        //[AllowAnonymous]
        public IActionResult Index()
        {
            UsersViewModel model = new UsersViewModel()
            {
                Users = _context.Users.Include(u => u.CreatedContracts).Include(u => u.ParticipatedContracts)/*.Where(u => u.UserName != User.Identity.Name)*/.ToList()
            };
            return View(model);
        }

        //init contract
        [HttpGet]
        public async Task<IActionResult> AddContract(string id, string type)
        {
            var partner = await _userManager.FindByIdAsync(id);
            var user = await _userManager.FindByEmailAsync(User.Identity.Name);
            TrustContract contract = new TrustContract()
            {
                CreatedAt = DateTime.Now,
                Creator = user,
                CreatorId = user.Id,
                Partner = partner,
                PartnerId = partner.Id,
                IsFulfilled = false,
                AtStep = ContractStep.ContractStepOne,
                Type = Enum.Parse<ContractType>(type),
            };
            await _context.TrustContracts.AddAsync(contract);
            await _context.SaveChangesAsync();

            user.CreatedContracts.Add(contract);
            await _userManager.UpdateAsync(user);
            _context.SaveChanges();
            partner.ParticipatedContracts.Add(contract);
            await _userManager.UpdateAsync(partner);
            _context.SaveChanges();

            ContractStepOneViewModel model = new ContractStepOneViewModel()
            {
                Users = _context.Users.Where(u => u.Email != User.Identity.Name).ToList(),
                Type = (ContractType)Enum.Parse(typeof(ContractType), type),
                PartnerId = id,
                PartnerName = partner.UserName,
                ContractId = contract.Id
            };

            return View(nameof(ContractStepOne),model);

        }

        //for further editing
        [HttpGet]
        public async Task<IActionResult> ContractStepOne(Guid contractId)
        {
            var contract = await _context.TrustContracts
                .Include(c => c.Creator)
                .Include(c => c.Partner)
                .Include(tc => tc.Changes)
                .ThenInclude(tc => tc.ContracFieldChanges)
                .ThenInclude(tc => tc.ContractChange)
                .Where(c => c.Id == contractId)
                .FirstOrDefaultAsync();

            var user = await _userManager.FindByEmailAsync(User.Identity.Name);

            ContractStepOneViewModel model = new ContractStepOneViewModel()
            {
                Users = _context.Users.Where(u => u.Email != User.Identity.Name).ToList(),
                Type = contract.Type,
                PartnerId = contract.PartnerId,
                PartnerName = contract.Partner.UserName,
                ContractId = contract.Id,
                CreatedAt = contract.CreatedAt,
                Description = contract.Description ?? default,
                Price = contract.Price,
                TEnum = contract.TEnum,
                VAT = contract.VAT
            };
            if (contract.HandoverCount > 1)
            {
                model.ChangedField = await GetChangedFields(contract);
                model.ChangedStep = await GetChangedSteps(contract);
            }

            _context.TrustContracts.Update(contract);
            contract.AtStep = ContractStep.ContractStepOne;
            await _context.SaveChangesAsync();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ContractStepOne(ContractStepOneViewModel model)
        {
            var user = await _userManager.FindByEmailAsync(User.Identity.Name);
            var contract =
                await _context.TrustContracts
                .Include(tc => tc.Changes)
                .ThenInclude(tc => tc.ContracFieldChanges)
                .ThenInclude(tc => tc.ContractChange)
                .Where(tc => tc.Id == model.ContractId)
                .FirstOrDefaultAsync();
            if (contract.HandoverCount > 1)
            {
                var changeSession = contract.Changes.OrderByDescending(x => x.CreatedAt).FirstOrDefault();

                if (changeSession != null && changeSession.Creator == user)
                {
                    if (model.TEnum != contract.TEnum)
                    {
                        await SaveChanges(changeSession, contract, model, nameof(TrustContract.TEnum));
                    }
                    if (model.Description != contract.Description)
                    {
                        await SaveChanges(changeSession, contract, model, nameof(TrustContract.Description));
                    }
                    if (model.Price != contract.Price)
                    {
                        await SaveChanges(changeSession, contract, model, nameof(TrustContract.Price));
                    }
                    if (model.VAT != contract.VAT)
                    {
                        await SaveChanges(changeSession, contract, model, nameof(TrustContract.VAT));
                    }
                }
            }
            else
            {
                _context.TrustContracts.Update(contract);
                contract.TEnum = model.TEnum;
                contract.Description = model.Description;
                contract.Price = model.Price;
                contract.VAT = model.VAT;
                await _context.SaveChangesAsync();
            }

            if (model.NextPage)
            {
                _context.TrustContracts.Update(contract);
                contract.AtStep += 1;
                await _context.SaveChangesAsync();
            }

            return RedirectToAction(contract.AtStep.ToString(), new { contractId = contract.Id });
        }

        [HttpGet]
        public async Task<IActionResult> ContractStepTwo(Guid contractId)
        {
            var contract = await _context.TrustContracts
                .Include(tc => tc.Partner)
                .Include(tc => tc.Changes)
                .ThenInclude(tc => tc.ContracFieldChanges)
                .ThenInclude(tc => tc.ContractChange)
                .Where(tc => tc.Id == contractId)
                .FirstOrDefaultAsync();

            ContractStepTwoViewModel model = new ContractStepTwoViewModel()
            {
                ContractId = contract.Id,
                CreatedAt = contract.CreatedAt,
                IsFultilled = contract.IsFulfilled,
                PartnerId = contract.PartnerId,
                PartnerName = contract.Partner.UserName,
                Type = contract.Type,
                ConfidentaInformation = contract.ConfidentaInformation ?? default,
                PaymentDate = contract.PaymentDate,
                TestBoolFieldTwo = contract.TestBoolFieldTwo
            };
            if (contract.HandoverCount > 1)
            {
                model.ChangedField = await GetChangedFields(contract);
                model.ChangedStep = await GetChangedSteps(contract);
            }

            _context.TrustContracts.Update(contract);
            contract.AtStep = ContractStep.ContractStepTwo;
            await _context.SaveChangesAsync();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ContractStepTwo(ContractStepTwoViewModel model)
        {
            var user = await _userManager.FindByEmailAsync(User.Identity.Name);
            var contract =
                await _context.TrustContracts
                .Include(tc => tc.Partner)
                .Include(tc => tc.Changes)
                .ThenInclude(tc => tc.ContracFieldChanges)
                .ThenInclude(tc => tc.ContractChange)
                .Where(tc => tc.Id == model.ContractId)
                .FirstOrDefaultAsync();
            if (contract.HandoverCount > 1)
            {
                var changeSession = contract.Changes.OrderByDescending(x => x.CreatedAt).FirstOrDefault();

                if (changeSession != null && changeSession.Creator == user)
                {
                    if (model.PaymentDate != contract.PaymentDate)
                    {
                        await SaveChanges(changeSession, contract, model, nameof(TrustContract.PaymentDate));
                    }
                    if (model.TestBoolFieldTwo != contract.TestBoolFieldTwo)
                    {
                        await SaveChanges(changeSession, contract, model, nameof(TrustContract.TestBoolFieldTwo));
                    }
                    if (model.ConfidentaInformation != contract.ConfidentaInformation)
                    {
                        await SaveChanges(changeSession, contract, model, nameof(TrustContract.ConfidentaInformation));
                    }
                }
            }
            else
            {
                _context.TrustContracts.Update(contract);
                contract.PaymentDate = model.PaymentDate;
                contract.TestBoolFieldTwo = model.TestBoolFieldTwo;
                contract.ConfidentaInformation = model.ConfidentaInformation;
                await _context.SaveChangesAsync();
            }

            if (model.NextPage)
            {
                _context.TrustContracts.Update(contract);
                contract.AtStep += 1;
                await _context.SaveChangesAsync();
            }

            return RedirectToAction(contract.AtStep.ToString(), new { contractId = contract.Id });
        }

        [HttpGet]
        public async Task<IActionResult> ContractStepThree(Guid contractId)
        {
            var contract = await _context.TrustContracts
                .Include(tc => tc.Partner)
                .Include(tc => tc.Changes)
                .ThenInclude(tc => tc.ContracFieldChanges)
                .ThenInclude(tc => tc.ContractChange)
                .Where(tc => tc.Id == contractId)
                .FirstOrDefaultAsync();

            ContractStepThreeViewModel model = new ContractStepThreeViewModel()
            {
                ContractId = contract.Id,
                CreatedAt = contract.CreatedAt,
                IsFultilled = contract.IsFulfilled,
                PartnerId = contract.PartnerId,
                PartnerName = contract.Partner.UserName,
                Type = contract.Type,
                FileName = contract.FileName ?? default
            };


            if (contract.HandoverCount > 1)
            {
                model.ChangedField = await GetChangedFields(contract);
                model.ChangedStep = await GetChangedSteps(contract);
            }
            _context.TrustContracts.Update(contract);
            contract.AtStep = ContractStep.ContractStepThree;
            await _context.SaveChangesAsync();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ContractStepThree(ContractStepThreeViewModel model)
        {
            var user = await _userManager.FindByEmailAsync(User.Identity.Name);
            var contract = await _context.TrustContracts
                .Include(tc => tc.Partner)
                .Include(tc => tc.Changes)
                .ThenInclude(tc => tc.ContracFieldChanges)
                .ThenInclude(tc => tc.ContractChange)
                .Where(tc => tc.Id == model.ContractId)
                .FirstOrDefaultAsync();

            if (contract.HandoverCount > 1)
            {
                var changeSession = contract.Changes.OrderByDescending(x => x.CreatedAt).FirstOrDefault();

                if (changeSession != null && changeSession.Creator == user)
                {
                    if (model.FileData.FileName != contract.FileName)
                    {
                        _context.ContractChanges.Update(changeSession);
                        changeSession.ContracFieldChanges.Add(new ContractFieldChange()
                        {
                            AtStep = ContractStep.ContractStepThree,
                            Field = nameof(contract.FileName),
                            OldValue = contract.FileName,
                            NewValue = model.FileData.FileName
                        });
                    }
                    if (model.FileData.ContentType != contract.FileContentType)
                    {
                        _context.ContractChanges.Update(changeSession);
                        changeSession.ContracFieldChanges.Add(new ContractFieldChange()
                        {
                            AtStep = ContractStep.ContractStepThree,
                            Field = nameof(contract.FileContentType),
                            OldValue = contract.FileContentType,
                            NewValue = model.FileData.ContentType
                        });
                    }

                }
            }
            if (model.FileData != null)
            {
                byte[] fileContentTmp = new byte[model.FileData.Length];

                using (var stream = model.FileData.OpenReadStream())
                {
                    stream.Read(fileContentTmp, 0, (int)model.FileData.Length);
                }

                _context.TrustContracts.Update(contract);
                contract.FileData = fileContentTmp;
                contract.FileName = model.FileData.FileName;
                contract.FileContentType = model.FileData.ContentType;

                await _context.SaveChangesAsync();
            }

            if (model.NextPage)
            {
                _context.TrustContracts.Update(contract);
                contract.AtStep += 1;
                await _context.SaveChangesAsync();
            }

            return RedirectToAction("ContractSummary", new { contractId = contract.Id });
        }


        public async Task<IActionResult> SendToPartner(Guid contractId)
        {
            var contract =
                await _context.TrustContracts
                .Include(x => x.Creator)
                .Include(x => x.Partner)
                .FirstOrDefaultAsync(c => c.Id == contractId);

            _context.TrustContracts.Update(contract);
            contract.Status = (ContractStatus)(((int)contract.Status + 1) % 2);
            contract.HandoverCount++;
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public async Task<IActionResult> ContractSummary(Guid contractId)
        {
            var contract = await _context.TrustContracts.Include(x => x.Creator).Include(x => x.Partner).FirstOrDefaultAsync(x => x.Id == contractId);

            ContractSummaryViewModel model = new ContractSummaryViewModel()
            {
                ContractId = contract.Id,
                FileName = contract.FileName,
                Status = contract.Status,
                TEnum = contract.TEnum,
                TestBoolField = contract.TestBoolFieldTwo,
                Type = contract.Type,
                Handovers = contract.HandoverCount,
                Creator = contract.Creator.Email,
                Partner = contract.Partner.Email
            };

            _context.TrustContracts.Update(contract);
            contract.AtStep = ContractStep.ContractSummary;
            await _context.SaveChangesAsync();

            return View(model);
        }


        private async Task<List<string>> GetChangedFields(TrustContract contract)
        {
            var user = await _userManager.FindByEmailAsync(User.Identity.Name);
            List<string> changedProps = new List<string>();
            var contractChage = contract.Changes.OrderByDescending(tc => tc.CreatedAt).FirstOrDefault(tc => tc.Creator != user);
            if (contractChage != null)
            {
                foreach (var changed in contractChage.ContracFieldChanges)
                {
                    if (contract.GetType().GetProperty(changed.Field) != null)
                    {
                        changedProps.Add(changed.Field);

                    }
                }
            }
            return changedProps;
        }

        private async Task<List<string>> GetChangedSteps(TrustContract contract)
        {
            var user = await _userManager.FindByEmailAsync(User.Identity.Name);
            List<string> changedSteps = new List<string>();
            var contractChage = contract.Changes.OrderByDescending(tc => tc.CreatedAt).FirstOrDefault(tc => tc.Creator != user);
            if (contractChage != null)
            {
                foreach (var changed in contractChage.ContracFieldChanges)
                {
                    if (!changedSteps.Contains(changed.AtStep.ToString()))
                    {
                        changedSteps.Add(changed.AtStep.ToString());
                    }
                }
            }
            return changedSteps;
        }

        private async Task SaveChanges(ContractChange change, TrustContract contract, AdditionalContractInfoViewModel model, string field)
        {
            _context.ContractChanges.Update(change);
            change.ContracFieldChanges.Add(new ContractFieldChange()
            {
                AtStep = contract.AtStep,
                Field = field,
                OldValue = contract.GetType().GetProperty(field).GetValue(contract).ToString(),
                NewValue = model.GetType().GetProperty(field).GetValue(model).ToString(),
                CreatedAt = DateTime.Now
            });

            await _context.SaveChangesAsync();
            _context.TrustContracts.Update(contract);
            contract.GetType().GetProperty(field).SetValue(
                contract,
                model.GetType().GetProperty(field).GetValue(model)
                );
            await _context.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<IActionResult> AddContract(AddContractViewModel model)
        {
            var user = await _userManager.FindByEmailAsync(User.Identity.Name);
            var partner = await _userManager.FindByIdAsync(model.PartnerId);
            var contract = await _context.TrustContracts.FindAsync(model.ContractId);
            byte[] fileData = new byte[model.FileData.Length];
            using (var stream = model.FileData.OpenReadStream())
            {
                stream.Read(fileData, 0, (int)model.FileData.Length);
            }

            _context.TrustContracts.Update(contract);

            contract.ConfidentaInformation = model.ConfidentalInformation;
            contract.Description = model.Description;
            contract.PaymentDate = model.PaymentDate;
            contract.Price = model.Price;
            contract.VAT = model.VAT;
            contract.TestBoolFieldTwo = model.TestBoolFieldTwo;
            contract.Changes = new List<ContractChange>();
            contract.FileData = fileData;
            contract.AtStep = ContractStep.ContractStepTwo;
            contract.FileName = model.FileData.FileName;

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public async Task<IActionResult> Contracts()
        {
            var user = await _userManager.FindByEmailAsync(User.Identity.Name);

            ContractsViewModel model = new ContractsViewModel()
            {
                TrustContracts = _context.TrustContracts
                .Include(c => c.Partner)
                .Include(tc => tc.Creator)
                .Include(tc => tc.Changes)
                .ThenInclude(tc => tc.ContracFieldChanges)
                .ThenInclude(tc => tc.ContractChange)
                .Where(tc => tc.Creator == user || tc.Partner == user).ToList()
            };
            return View(model);

        }

        [HttpGet]
        public async Task<IActionResult> EditContract(Guid id)
        {
            TrustContract contract =
                await _context.TrustContracts
                .Include(p => p.Partner)
                .Include(c => c.Creator)
                .Include(x => x.Changes)
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();
            var user = await _userManager.FindByEmailAsync(User.Identity.Name);

            _context.TrustContracts.Update(contract);
            contract.Changes.Add(new ContractChange()
            {
                CreatedAt = DateTime.Now,
                Creator = user,
                CreatorId = user.Id
            });

            await _context.SaveChangesAsync();

            return RedirectToAction(Enum.GetName(typeof(ContractStep), (ContractStep)((int)contract.AtStep % 4)), new { contractId = contract.Id });
        }

        public async Task<IActionResult> ChangeLog(Guid id)
        {
            var contract = await _context.TrustContracts
                .Include(tc => tc.Changes)
                .ThenInclude(tc => tc.ContracFieldChanges)
                .ThenInclude(tc => tc.ContractChange)
                .Where(tc => tc.Id == id)
                .FirstOrDefaultAsync();

            List<ContractFieldChange> changes = new List<ContractFieldChange>();

            foreach (var change in contract.Changes)
            {
                foreach (var prop in contract.GetType().GetProperties())
                {
                    var changeTmp = change.ContracFieldChanges.Where(f => f.Field == prop.Name).OrderBy(x => x.CreatedAt).FirstOrDefault();
                    if (changeTmp != null)
                    {
                        changeTmp.NewValue = prop.GetValue(contract).ToString();
                        changes.Add(changeTmp);
                    }
                }
            }

            return View(changes);
        }

        //[HttpPost]
        //public async Task<IActionResult> EditContract(EditContractViewModel model)
        //{
        //    var contract = await _context.TrustContracts.FindAsync(model.ContractId);
        //    bool changePresent = false;
        //    ContractChange newChange = new ContractChange();
        //    model.ContentType = model.FileDataForm.ContentType;
        //    model.FileName = model.FileDataForm.FileName;

        //    model.FileData = new byte[model.FileDataForm.Length];
        //    using (var stream = model.FileDataForm.OpenReadStream())
        //    {
        //        stream.Read(model.FileData, 0, model.FileData.Length);
        //    }

        //    // diff
        //    foreach (var prop in model.GetType().GetProperties())
        //    {
        //        // contains
        //        if (contract.GetType().GetProperty(prop.Name) != null)
        //        {
        //            var newVal = prop.GetValue(model) == null ? string.Empty : Convert.ChangeType(prop.GetValue(model), prop.GetValue(model).GetType());
        //            var oldVal = contract.GetType().GetProperty(prop.Name).GetValue(contract) == null ? string.Empty : Convert.ChangeType(contract.GetType().GetProperty(prop.Name).GetValue(contract), contract.GetType().GetProperty(prop.Name).GetValue(contract).GetType());
        //            if (newVal.ToString() != oldVal.ToString())
        //            {
        //                _context.TrustContracts.Update(contract);
        //                var user = await _userManager.FindByEmailAsync(User.Identity.Name);

        //                if (!changePresent)
        //                {
        //                    newChange.CreatedAt = DateTime.Now;
        //                    newChange.Creator = user;
        //                    newChange.ContractId = contract.Id;
        //                    newChange.ContracFieldChanges = new List<ContractFieldChange>();
        //                }

        //                newChange.ContracFieldChanges.Add(new ContractFieldChange()
        //                {
        //                    Field = prop.Name,
        //                    NewValue = newVal.ToString(),
        //                    OldValue = oldVal.ToString()
        //                });

        //                contract.GetType().GetProperty(prop.Name).SetValue(contract, newVal);
        //                _context.SaveChanges();

        //                _context.ContractChanges.Add(newChange);
        //                _context.SaveChanges();
        //                changePresent = true;
        //            }
        //        }
        //    }
        //    return RedirectToAction(nameof(Index));
        //}




        //----------------------------------------------------------------------------
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
