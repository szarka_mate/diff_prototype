﻿using DiffPrototype.Data;
using DiffPrototype.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DiffPrototype.Areas.Identity.Data
{
    public class ApplicationUser : IdentityUser
    {
        public ICollection<TrustContract> CreatedContracts { get; set; }
        public ICollection<TrustContract> ParticipatedContracts { get; set; }
    }
}
